# SpeechRecognitionView

#### 项目介绍
- 项目名称：SpeechRecognitionView
- 所属系列：openharmony的第三方组件适配移植
- 功能：一种动画通过语音来控制变化
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Releases 1.2.2

#### 效果演示
<img src="img/demo.gif"></img>

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:SpeechRecognitionView:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

使用该库非常简单，只需查看提供的示例的源代码。
```示例XML
 <com.github.zagum.speechrecognitionview.RecognitionProgressView
            ohos:id="$+id:recognition_view"
            ohos:height="42vp"
            ohos:width="100vp"
            ohos:center_in_parent="true"
            ohos:layout_alignment="center"/>``
```

```java
         int[] colors = {
                Color.getIntColor("#3164d7"),
                Color.getIntColor("#d92d29"),
                Color.getIntColor("#eeaa10"),
                Color.getIntColor("#3164d7"),
                Color.getIntColor("#2e9641")
        };
        int[] heights = {20, 24, 18, 23, 16};
        initIntent = new AsrIntent();
        initIntent.setAudioSourceType(AsrIntent.AsrAudioSrcType.ASR_SRC_TYPE_PCM);
        initIntent.setFilePath(this.getAbilityPackageContext().getExternalCacheDir().getAbsolutePath());
        recognitionProgressView = (RecognitionProgressView) findComponentById(ResourceTable.Id_recognition_view);
        recognitionProgressView.setRecognitionListener(new RecognitionListenerAdapter() {
            @Override
            public void onResults(PacMap results) {
                showResults(results);
            }
        });
         recognitionProgressView.setColors(colors);
         recognitionProgressView.setBarMaxHeightsInDp(heights);
         recognitionProgressView.setCircleRadiusInDp(2);
         recognitionProgressView.setSpacingInDp(2);
         recognitionProgressView.setIdleStateAmplitudeInDp(2);
         recognitionProgressView.setRotationRadiusInDp(10);
         recognitionProgressView.play();
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

        Copyright 2016 Evgenii Zagumennyi
        
        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at
        
        http://www.apache.org/licenses/LICENSE-2.0
        
        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and
        limitations under the License.
