package com.github.zagum.speechrecognitionview.sample;

import com.github.zagum.speechrecognitionview.RecognitionProgressView;
import com.github.zagum.speechrecognitionview.util.DensityUtils;
import ohos.agp.text.Font;
import ohos.app.Context;
import org.junit.Test;

public class RecognitionProgressViewTest {

    @Test
    public void setSpeechRecognizer() {
        DensityUtils densityUtils = new DensityUtils();
        Font font = densityUtils.font("/com.github.zagum.speechrecognitionview.sample");
        System.out.println(font);
    }

    @Test
    public void setRecognitionListener() {
        DensityUtils densityUtils = new DensityUtils();
        Font font = densityUtils.font("bottif.tff");
        System.out.println(font);
    }

    @Test
    public void play() {
        DensityUtils densityUtils = new DensityUtils();
        Font font = densityUtils.font("bottif.tff");
        System.out.println(font);
    }

    @Test
    public void stop() {
        DensityUtils densityUtils = new DensityUtils();
        Font font = densityUtils.font("bottif.tff");
        System.out.println(font);
    }

    @Test
    public void setSingleColor() {
        DensityUtils densityUtils = new DensityUtils();
        Font font = densityUtils.font("bottif.tff");
        System.out.println(font);
    }

    @Test
    public void setColors() {
        DensityUtils densityUtils = new DensityUtils();
        Font font = densityUtils.font("bottif.tff");
        System.out.println(font);
    }

    @Test
    public void setBarMaxHeightsInDp() {
        DensityUtils densityUtils = new DensityUtils();
        Font font = densityUtils.font("bottif.tff");
        System.out.println(font);
    }

    @Test
    public void setCircleRadiusInDp() {
        DensityUtils densityUtils = new DensityUtils();
        Font font = densityUtils.font("bottif.tff");
        System.out.println(font);
    }

    @Test
    public void setSpacingInDp() {
        DensityUtils densityUtils = new DensityUtils();
        Font font = densityUtils.font("bottif.tff");
        System.out.println(font);
    }

    @Test
    public void setIdleStateAmplitudeInDp() {
        DensityUtils densityUtils = new DensityUtils();
        Font font = densityUtils.font("bottif.tff");
        System.out.println(font);
    }

    @Test
    public void setRotationRadiusInDp() {
        DensityUtils densityUtils = new DensityUtils();
        Font font = densityUtils.font("bottif.tff");
        System.out.println(font);
    }
}