/*
 * Copyright (C) 2016 Evgenii Zagumennyi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.zagum.speechrecognitionview.animators;

import com.github.zagum.speechrecognitionview.RecognitionBar;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.utils.Point;

import java.util.ArrayList;
import java.util.List;

public class RotatingAnimator implements BarParamsAnimator {

    private static final long DURATION = 2000;
    private static final long ACCELERATE_ROTATION_DURATION = 1000;
    private static final long DECELERATE_ROTATION_DURATION = 1000;
    private static final float ROTATION_DEGREES = 720f;
    private static final float ACCELERATION_ROTATION_DEGREES = 40f;

    private long startTimestamp;
    private boolean isPlaying;

    private final int centerX, centerY;
    private final List<Point> startPositions;
    private final List<RecognitionBar> bars;

    public RotatingAnimator(List<RecognitionBar> bars, int centerX, int centerY) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.bars = bars;
        this.startPositions = new ArrayList<>();
        for (RecognitionBar bar : bars) {
            startPositions.add(new Point(bar.getX(), bar.getY()));
        }
    }

    @Override
    public void start() {
        isPlaying = true;
        startTimestamp = System.currentTimeMillis();
    }

    @Override
    public void stop() {
        isPlaying = false;
    }

    @Override
    public void animate() {
        System.out.println("animate--isPlaying>"+isPlaying);

        if (!isPlaying) return;

        long currTimestamp = System.currentTimeMillis();
        if (currTimestamp - startTimestamp > DURATION) {
            startTimestamp += DURATION;
        }

        long delta = currTimestamp - startTimestamp;

        float interpolatedTime = (float) delta / DURATION;

        float angle = interpolatedTime * ROTATION_DEGREES;

        System.out.println("barsize-->"+bars.size());
        for (int j = 0;j<bars.size();j++) {

            float finalAngle = angle;
            if (j > 0 && delta > ACCELERATE_ROTATION_DURATION) {
                System.out.println("finalAngle-->"+"ACCELERATE_ROTATION_DURATION)");

                finalAngle += decelerate(delta, bars.size() - j);
                System.out.println("finalAnglefinalAngle-->"+finalAngle);

            } else if (j > 0) {
                System.out.println("finalAngle-->"+"elsej");

                finalAngle += accelerate(delta, bars.size() - j);
            }
            rotate(bars.get(j), finalAngle, startPositions.get(j));

        }
    }

    private float decelerate(long delta, int scale) {
        long accelerationDelta = delta - ACCELERATE_ROTATION_DURATION;
        float interpolatedTime = getInterpolation((float) accelerationDelta / DECELERATE_ROTATION_DURATION);
        float decelerationAngle = -interpolatedTime * (ACCELERATION_ROTATION_DEGREES * scale);
        System.out.println("decelerate---->"+interpolatedTime + "--->"+decelerationAngle);
        return ACCELERATION_ROTATION_DEGREES * scale + decelerationAngle;
    }

    private float accelerate(long delta, int scale) {
        long accelerationDelta = delta;
        float interpolatedTime = getInterpolation((float) accelerationDelta / ACCELERATE_ROTATION_DURATION);
        float accelerationAngle = interpolatedTime * (ACCELERATION_ROTATION_DEGREES * scale);
        System.out.println("accelerate---->"+interpolatedTime + "--->"+accelerationAngle);

        return accelerationAngle;
    }
    private float getInterpolation(float input) {
        return (float)(Math.cos((input + 1) * Math.PI) / 2.0f) + 0.5f;
    }


    private void rotate(RecognitionBar bar, double degrees, Point startPosition) {

        double angle = Math.toRadians(degrees);

        int x = centerX + (int) ((startPosition.getPointX() - centerX) * Math.cos(angle) -
                (startPosition.getPointY() - centerY) * Math.sin(angle));

        int y = centerY + (int) ((startPosition.getPointX() - centerX) * Math.sin(angle) +
                (startPosition.getPointY() - centerY) * Math.cos(angle));

        bar.setX(x);
        bar.setY(y);
        bar.update();
    }
}