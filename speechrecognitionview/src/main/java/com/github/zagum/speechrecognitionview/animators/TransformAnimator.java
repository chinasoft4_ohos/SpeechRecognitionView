/*
 * Copyright (C) 2016 Evgenii Zagumennyi
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.zagum.speechrecognitionview.animators;

import com.github.zagum.speechrecognitionview.RecognitionBar;
import com.github.zagum.speechrecognitionview.RecognitionProgressView;
import ohos.agp.utils.Point;

import java.util.ArrayList;
import java.util.List;

public class TransformAnimator implements BarParamsAnimator {

    private static final long DURATION = 300;

    private long startTimestamp;
    private boolean isPlaying;


    private OnInterpolationFinishedListener listener;

    private final int radius;
    private final int centerX, centerY;
    private final List<Point> finalPositions = new ArrayList<>();
    private final List<RecognitionBar> bars;

    public TransformAnimator(List<RecognitionBar> bars, int centerX, int centerY, int radius) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.bars = bars;
        this.radius = radius;
    }

    @Override
    public void start() {
        isPlaying = true;
        startTimestamp = System.currentTimeMillis();
        initFinalPositions();
    }

    @Override
    public void stop() {
        isPlaying = false;
        if (listener != null) {
            listener.onFinished();
        }
    }

    @Override
    public void animate() {
        if (!isPlaying) return;

        long currTimestamp = System.currentTimeMillis();
        long delta = currTimestamp - startTimestamp;
        if (delta > DURATION) {
            delta = DURATION;
        }
        try {
            for (int i = 0; i < bars.size(); i++) {
                RecognitionBar bar = bars.get(i);

                int x = bar.getStartX() + (int) ((finalPositions.get(i).getPointX() - bar.getStartX()) * ((float) delta / DURATION));
                int y = bar.getStartY() + (int) ((finalPositions.get(i).getPointY() - bar.getStartY()) * ((float) delta / DURATION));

                bar.setX(x);
                bar.setY(y);
                bar.update();
            }


        }catch (Exception e){
            e.fillInStackTrace();
        }

        if (delta == DURATION) {
            stop();
        }
    }

    private void initFinalPositions() {
        Point startPoint = new Point(centerX,centerY - radius);
        for (int i = 0; i < RecognitionProgressView.BARS_COUNT; i++) {
            Point point = new Point(startPoint);
            rotate((360d / RecognitionProgressView.BARS_COUNT) * i, point);
            finalPositions.add(point);
        }
    }


    private void rotate(double degrees, Point point) {

        double angle = Math.toRadians(degrees);

        int x = centerX + (int) ((point.getPointX() - centerX) * Math.cos(angle) -
                (point.getPointY() - centerY) * Math.sin(angle));

        int y = centerY + (int) ((point.getPointX() - centerX) * Math.sin(angle) +
                (point.getPointY() - centerY) * Math.cos(angle));

        point.modify(x,y);
    }

    public void setOnInterpolationFinishedListener(OnInterpolationFinishedListener listener) {
        this.listener = listener;
    }

    public interface OnInterpolationFinishedListener {
        void onFinished();
    }
}