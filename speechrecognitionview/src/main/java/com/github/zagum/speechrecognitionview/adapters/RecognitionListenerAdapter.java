package com.github.zagum.speechrecognitionview.adapters;


import ohos.ai.asr.AsrListener;
import ohos.utils.PacMap;

public abstract class RecognitionListenerAdapter implements AsrListener {
    @Override
    public void onInit(PacMap pacMap) {

    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onRmsChanged(float v) {

    }

    @Override
    public void onBufferReceived(byte[] bytes) {

    }

    @Override
    public void onEndOfSpeech() {

    }

    @Override
    public void onError(int i) {

    }

    @Override
    public void onResults(PacMap pacMap) {

    }

    @Override
    public void onIntermediateResults(PacMap pacMap) {

    }

    @Override
    public void onEnd() {

    }

    @Override
    public void onEvent(int i, PacMap pacMap) {

    }

    @Override
    public void onAudioStart() {

    }

    @Override
    public void onAudioEnd() {

    }
}
